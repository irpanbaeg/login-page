   <nav class="navbar navbar-expand-lg bg">
        <div class="container-fluid my-4">
            <img class="image" src="<?php echo base_url('assets/'); ?>images/Ellipse8.png">
            <img class="image1" src="<?php echo base_url('assets/'); ?>images//Ellipse7.png">
            <div class="text-navbar">
                <p><b>Camp <p style="color: #13F0E3">Coding</p></b></p>
            </div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav m-auto">
                    <li class="nav-item px-2 mx-2">
                        <a class="nav-link text-white mx-2" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item mx-2 px-2">
                        <a class="nav-link text-white" href="#">About</a>
                    </li>
                    <li class="nav-item mx-2 px-2">
                        <a class="nav-link text-white" href="#">Contact</a>
                    </li>
                    <li class="nav-item mx-4 px-2 ">
                        <button type="button" class="btn btn-blue">Login</button>
                    </li>
                    <li class="nav-item mx-2 px-2">
                    <a href="<?php echo base_url('index.php/auth/register'); ?>" type="button" class="btn btn-white">Daftar</a> </button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="d-lg-flex half">
        <div class="bg order-1 order-md-2">
            <div class="img-login">
                <img src="<?php echo base_url('assets/'); ?>images/login.png">
            </div>
        </div>
        <div class="contents order-2 order-md-1">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-8">
                        <div class="content-container">
                            <img class="img" src="<?php echo base_url('assets/'); ?>images/Ellipse8.png">
                            <img class="img1" src="<?php echo base_url('assets/'); ?>images/Ellipse7.png">
                            <div class="text-p">
                                <h3>
                                    <strong>Camp <p style="color: #13F0E3">Coding</p></strong>
                                </h3>
                            </div>
                            <h3><strong>Login</strong></h3>
                            <p class="text-login">Login to Continue Our Application</p>
                            <form action="#" method="post">
                                <div class="form-group first">
                                    <div class="img-email">
                                        <img src="<?php echo base_url('assets/'); ?>images/email.png">
                                    </div>
                                    <input type="text" class="form-control" placeholder="Email" id="username">
                                </div>
                                <div class="form-group last mb-3">
                                    <div class="img-hide">
                                        <img src="<?php echo base_url('assets/'); ?>images/hide.png">
                                    </div>
                                    <input type="password" class="form-control" placeholder="Password" id="password">
                                </div>
                                <div class="d-flex mb-4 align-items-center">
                                    <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span>
                                </div>
                                <input style="background: linear-gradient(89.12deg, #966AA2 1.22%, #11C4CF 100.87%);" type="submit" value="Login" class="btn btn-block btn-primary">
                                <h6>Belum mempunyai akun ? <a href="<?php echo base_url('index.php/auth/register'); ?>" style="color : orange">Daftar sekarang</h6>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        